# Select-Tree

#### 在线预览
[在线预览](http://leeatao.gitee.io/select-tree/select-tree/select-tree.html)
#### 介绍
基于Jquery的下拉树插件，根据select的option值，自动组装树结构。可灵活应用于项目中，不需要再单独引用其他插件实现简单的多级下拉选择。


#### 安装教程

1.  下载插件包解压。
2.  将select-tree.js和select-tree.css引入页面。
3.  根据使用示例调用即可。

#### 使用说明

1.  引入js和css文件。
2.  编写原生的select组件满足插件需要的数据元素。
3.  使用jquery绑定插件，实现效果。
#### 使用示例

```
<select data-placeholder="请选择组织架构" id="tree1" name="tree1" data-msg="请选择组织架构">
            <option value=""></option>
            <option value="1" data-pid="0">一级节点1</option>
            <option value="12" data-pid="1">子节点12</option>
            <option value="123" data-pid="12">子节点123</option>
            <option value="13" data-pid="1">子节点13</option>
            <option value="131" data-pid="13">子节点131</option>
            <option value="132" data-pid="13">子节点132</option>
            <option value="133" data-pid="13">子节点13</option>
            <option value="134" data-pid="13">子节点134</option>
            <option value="135" data-pid="13">子节点135</option>
            <option value="1341" data-pid="134">子节点1341</option>
            <option value="2" data-pid="0">一级节点2</option>
            <option value="3" data-pid="0">一级节点3</option>
            <option value="21" data-pid="2">子节点21</option>
        </select>

```

```
 $("#tree1).selectTree({
            expandAll: true,//是否全部展开，如果为tree，expandLevel不生效
            expandLevel: 0//展开层级
 });

```


#### 效果图片
    
![输入图片说明](https://images.gitee.com/uploads/images/2020/1207/163243_8d13bc59_5597419.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1207/163255_681a0e25_5597419.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1207/163314_4a8b820c_5597419.png "屏幕截图.png")